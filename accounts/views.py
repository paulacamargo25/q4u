from django.conf import settings
from django.core.mail import send_mail
from django.shortcuts import get_object_or_404
from django.utils.translation import ugettext_lazy as _

from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet, mixins, ModelViewSet
from rest_framework.mixins import (
    CreateModelMixin,
)
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework import status

from allauth.utils import generate_unique_username

from .models import Device, EmailUser, EmailEnterprise
from .serializers import (
    ChangePasswordSerializer,
    DeviceSerializer,
    EmailUserRegisterSerializer,
    EnterpriseSerializer,
    EnterpriseAuthTokenSerializer
)


class RestoreAuthToken(APIView):
    permission_classes = (AllowAny, )

    def post(self):
        auth_token = getattr(self.request.user, 'auth_token', None)
        if auth_token:
            auth_token.delete()
            return Response()
        return Response(status=status.HTTP_401_UNAUTHORIZED)


restore_auth_token = RestoreAuthToken.as_view()


class RegisterUserViewSet(GenericViewSet, CreateModelMixin):
    """
    ViewSet that includes the user registration view
    """

    serializer_class = EmailUserRegisterSerializer

    def perform_create(self, serializer):
        username = generate_unique_username(
            [serializer.validated_data['first_name'],
             serializer.validated_data['last_name'],
             serializer.validated_data['email']])
        serializer.validated_data['username'] = username
        user = serializer.save()
        user.set_password(serializer.data['password'])
        user.save()


class LoginViewSet(GenericViewSet, CreateModelMixin):
    """
    ViewSet that includes the user login view
    """

    serializer_class = AuthTokenSerializer

    def create(self, request, *args, **kwargs):
        """
        Validate the user and returns a token

        :param request: request information
        :return: Response with the user token
        :rtype Response
        """

        serializer = self.serializer_class(
            data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, c = Token.objects.get_or_create(user=user)
        return Response({'token': token.key})

    def post_create(self):
        """
        Prevents creation of a new user when login in
        """

        pass


class LoginEnterpriseViewSet(GenericViewSet, CreateModelMixin):
    """
    ViewSet that includes the user login view
    """

    serializer_class = EnterpriseAuthTokenSerializer

    def create(self, request, *args, **kwargs):
        """
        Validate the user and returns a token

        :param request: request information
        :return: Response with the user token
        :rtype Response
        """

        serializer = self.serializer_class(
            data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, c = Token.objects.get_or_create(user=user)
        return Response({'token': token.key})

    def post_create(self):
        """
        Prevents creation of a new user when login in
        """

        pass


class DeviceViewSet(GenericViewSet, mixins.CreateModelMixin):
    """
    Model View that includes the user device  information
    """

    permission_classes = (IsAuthenticated,)
    serializer_class = DeviceSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        device = serializer.save(user=self.request.user)
        return Response(
            self.get_serializer(device).data,
            status=status.HTTP_201_CREATED
        )


class LogoutViewSet(APIView):
    """
    Logout ViewSet that remove the device associated to the user
    """

    permission_classes = (IsAuthenticated,)

    def post(self, request):
        device_id = request.data.get('device_id', None)
        if device_id:
            get_object_or_404(Device, pk=device_id).delete()

        return Response(status=status.HTTP_204_NO_CONTENT)


logout = LogoutViewSet.as_view()


class ChangePasswordViewSet(GenericViewSet, mixins.CreateModelMixin):

    serializer_class = ChangePasswordSerializer
    permission_classes = (IsAuthenticated,)
    queryset = EmailUser.objects.all()

    def get_object(self):
        return self.request.user

    def perform_create(self, serializer):
        """
        After change the passord email the user
        """

        user = serializer.save()
        user.set_password(serializer.validated_data['new_password'])
        user.save()
        send_mail(
            from_email=settings.EMAIL_FROM,
            subject=_("Password change"),
            message=_("Your password has been changed successfully"),
            recipient_list=[user.email]
        )


class EnterpriseViewSet(ModelViewSet):
    serializer_class = EnterpriseSerializer
    queryset = EmailEnterprise.objects.all()

