from datetime import date

from django.utils.translation import ugettext_lazy as _

PLATFORM_IOS = 'ios'
PLATFORM_ANDROID = 'android'
PLATFORM_OTHER = 'other'

PLATFORM_CHOICES = (
    (PLATFORM_ANDROID, _('Android')),
    (PLATFORM_IOS, _('iOS')),
    (PLATFORM_OTHER, _('Other'))
)

DEFAULT_DATE = date(2018, 1, 1)
