from django.contrib.auth.models import AbstractUser, AbstractBaseUser
from django.db import models
from django.utils.translation import gettext_lazy as _

from django_extensions.db.models import TimeStampedModel

from .managers import UserManager
from .constants import (
    PLATFORM_IOS, PLATFORM_ANDROID, PLATFORM_OTHER, PLATFORM_CHOICES,
    DEFAULT_DATE)


class EmailUser(AbstractUser, TimeStampedModel):
    """
    User Model made for the User who has no username and instead has an Email

    :cvar email: EmailField that stores user email
    :cvar first_name: CharField that stores user first name
    :cvar last_name: CharField that stores user last name
    :cvar is_active: BooleanField that stores if user is active
    :cvar is_admin: BooleanField that stores if user is an admin
    :cvar is_staff: BooleanField that stores if user is part of the staff
    """

    email = models.EmailField(verbose_name=_('email address'), unique=True)
    first_name = models.CharField(
        verbose_name=_('first name'),
        max_length=64,
        null=True,
        blank=True
    )
    last_name = models.CharField(
        verbose_name=_('last name'),
        max_length=64,
        null=True,
        blank=True
    )
    username = models.CharField(
        verbose_name=_('username'),
        max_length=128,
        unique=True
    )

    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_enterprise = models.BooleanField(default=False)


    objects = UserManager()

    def __str__(self):
        return self.email

    class Meta:
        verbose_name = _('User')
        verbose_name_plural = _('Users')

    @property
    def get_short_name(self):
        """
        Method made to return first name, needed on Admin main page
        """

        return self.first_name

    @property
    def get_full_name(self):
        """
        Function returns first name and last name, used by the Admin directly
        """

        return '{first_name} {last_name}'.format(
            first_name=self.first_name, last_name=self.last_name)


class EmailEnterprise(models.Model):
    """
    User Model made for the Enterprise who has no username and instead has an Email

    :cvar email: EmailField that stores enterprise email
    :cvar name: CharField that stores enterprise name
    :cvar city: CharField that stores enterprise city
    :cvar country: CharField that stores enterprise country
    :cvar address: CharField that stores enterprise address
    :cvar is_active: BooleanField that stores if enterprise is active

    """

    email = models.EmailField(verbose_name=_('email address'), unique=True)
    name = models.CharField(
        verbose_name=_('name'),
        max_length=64,
        null=True,
        blank=True
    )
    city = models.CharField(
        verbose_name=_('city'),
        max_length=64,
        null=True,
        blank=True
    )
    country = models.CharField(
        verbose_name=_('country'),
        max_length=128,
    )
    address = models.CharField(
        verbose_name=_('address'),
        max_length=128,
    )

    is_active = models.BooleanField(default=True)

    user = models.ForeignKey(EmailUser, verbose_name=_('user'),
                             on_delete=models.CASCADE, blank=True, null=True
                             )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Enterprise')
        verbose_name_plural = _('Enterprises')


class Device(models.Model):
    """
    Model that stores the Device information of the user

    :cvar token: charField that stores the ExpoPushToken
    :cvar system: charField that stores the device operating system
    :cvar user: ForeignKey to the related user.
    """

    ANDROID = PLATFORM_ANDROID
    IOS = PLATFORM_IOS
    OTHER = PLATFORM_OTHER

    token = models.CharField(max_length=128)
    platform = models.CharField(
        max_length=8, choices=PLATFORM_CHOICES, default=OTHER)
    user = models.ForeignKey(EmailUser, models.CASCADE)

    class Meta:
        verbose_name = _('Device')
        verbose_name_plural = _('Devices')
        unique_together = ('token', 'user')
