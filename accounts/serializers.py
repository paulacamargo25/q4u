import django.contrib.auth.password_validation as validators

from django.core import exceptions
from django.utils.translation import ugettext_lazy as _

from rest_framework import serializers
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.compat import authenticate

from .models import EmailUser, Device, EmailEnterprise


class EnterpriseSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmailEnterprise
        fields = ('id', 'email', 'name', 'city', 'country', 'address')


class EnterpriseAuthTokenSerializer(AuthTokenSerializer):
    def validate(self, attrs):
        username = attrs.get('username')
        password = attrs.get('password')

        if username and password:
            user = authenticate(request=self.context.get('request'),
                                username=username, password=password)

            # The authenticate call simply returns None for is_active=False
            # users. (Assuming the default ModelBackend authentication
            # backend.)
            if not user:
                msg = _('Unable to log in with provided credentials.')
                raise serializers.ValidationError(msg, code='authorization')
        else:
            msg = _('Must include "username" and "password".')
            raise serializers.ValidationError(msg, code='authorization')
        if not user.is_enterprise:
            msg = _('You dony have enterpriser.')
            raise serializers.ValidationError(msg, code='authorization')

        attrs['user'] = user
        return attrs


class UserSerializer(serializers.ModelSerializer):
    """
    Serializer for Retrieve User Data
    """

    class Meta:
        model = EmailUser
        fields = ('first_name', 'last_name', 'email', 'last_login')


class ChangePasswordSerializer(serializers.Serializer):
    """
    Serializer for change user password
    """

    current_password = serializers.CharField()
    new_password = serializers.CharField()

    def validate_current_password(self, current_password):
        """
        Check that the current password is from the current user
        """

        user = self.context['request'].user
        if user.check_password(current_password):
            return current_password
        raise serializers.ValidationError(
            _("Incorrect password for current user"))

    def validate_new_password(self, new_password):
        """
        Check that the new password is valid
        """

        try:
            validators.validate_password(
                password=new_password, user=self.context['request'].user)
        except exceptions.ValidationError as e:
            raise serializers.ValidationError(e.messages)
        else:
            return new_password

    def validate(self, passwords):
        """
        Validate that the new password is different from the old one
        """

        if passwords['current_password'] == passwords['new_password']:
            raise serializers.ValidationError(
                _("New password cannot be the same as old"))
        return passwords

    def save(self):
        """
        Assign the new password to the user
        """
        return self.context['request'].user

    class Meta:
        fields = ('current_password', 'new_password')


class EmailUserRegisterSerializer(serializers.ModelSerializer):
    """
    Serializer to create a new user
    """
    email = serializers.EmailField()

    def valid_email(self, value):
        return value.lower()

    def validate(self, data):
        """
        Check that the start is before the stop.
        """

        if EmailUser.objects.filter(email=data['email']).exists():
            raise serializers.ValidationError(
                _("Email already exists"))

        user = EmailUser(**data)
        password = data.get('password')
        errors = dict()
        try:
            validators.validate_password(password=password, user=user)
        except exceptions.ValidationError as e:
            errors['password'] = list(e.messages)

        if errors:
            raise serializers.ValidationError(errors)

        return data

    class Meta:
        model = EmailUser
        fields = ('email', 'password', 'first_name', 'last_name')


class DeviceSerializer(serializers.ModelSerializer):
    """
    Serializer for Device Model
    """

    def save(self, user, **kwargs):
        """
        Create the token only if it doesn't exist
        """

        device, c = Device.objects.get_or_create(
            user=user, token=self.validated_data['token'],
            defaults={'platform': self.validated_data.get(
                'platform', Device.OTHER)})
        return device

    class Meta:
        model = Device
        fields = ('id', 'token', 'platform')
        read_only = ('id', )
