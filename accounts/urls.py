from rest_framework.routers import DefaultRouter
from django.urls import path

from .views import (
    LoginViewSet, RegisterUserViewSet, DeviceViewSet, ChangePasswordViewSet,
    EnterpriseViewSet, LoginEnterpriseViewSet
)

app_name = 'accounts'

router = DefaultRouter()
router.register(r'login', LoginViewSet, base_name='login')
router.register(
    r'change-password',
    ChangePasswordViewSet,
    base_name='change-password'
)
router.register(r'register', RegisterUserViewSet, base_name='register')
router.register(r'devices', DeviceViewSet, base_name='devices')
router.register(r'enterprises', EnterpriseViewSet, base_name='enterprises')

router.register(r'loginEnterprise', LoginEnterpriseViewSet, base_name='login-enterprise')


urlpatterns = router.urls
