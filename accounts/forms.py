from django.utils.translation import gettext_lazy as _

from django import forms
from django.contrib.auth.forms import UserCreationForm

from .models import EmailUser



class EmailUserCreationForm(UserCreationForm):
    email = forms.CharField()
    password1 = forms.CharField(widget=forms.PasswordInput())
    password2 = forms.CharField(widget=forms.PasswordInput())

    class Meta(UserCreationForm.Meta):
        model = EmailUser
        fields = (
            'first_name',
            'last_name',
            'email',
            'password1',
            'password2'
        )

    def clean_password1(self):
        password = self.cleaned_data.get('password1')
        if len(password) < 6:
            raise forms.ValidationError(
                _('accounts.validators.password_min_length'))
        return password

    def clean_password2(self):
        password = self.cleaned_data.get('password1')
        password_check = self.cleaned_data.get('password2')
        if not password_check:
            raise forms.ValidationError(
                _('accounts.validators.invalid_password'))
        if password != password_check:
            raise forms.ValidationError(
                _('accounts.validators.passwords_doesnt_match'))
        return password_check
