from django.contrib.auth.base_user import BaseUserManager
from django.utils import timezone
from allauth.utils import generate_unique_username


class UserManager(BaseUserManager):
    """
    Class made to use for the creation of the super user and users
    """

    def create_user(self, email, password, **extra_fields):
        """
        Creates a new Email User

        :param email: User email
        :param password: User password
        :param extra_fields: First name, last name, phone number, company

        :return: User Instance
        """

        if not email:
            raise ValueError('The given email address must be set')
        user = self.model(
            email=email, is_staff=False, is_active=True, **extra_fields)
        user.set_password(password)
        user.date_joined = timezone.now()
        user.username = generate_unique_username(
            [user.first_name, user.last_name, user.email])
        user.save()

        return user

    def create_superuser(self, email, password, **extra_fields):
        """
        Creates a new Email User which is staff and is superuser

        :param email: Email of the user
        :param password: Password of the user
        :param extra_fields: extra fields of the user

        :return: User Instance
        """

        u = self.create_user(email, password, **extra_fields)
        u.is_staff = True
        u.is_active = True
        u.is_superuser = True
        u.user_type = 'admin'
        u.save(using=self._db)

        return u
