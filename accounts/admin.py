from django.contrib import admin

from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext_lazy as _

from .forms import EmailUserCreationForm
from .models import EmailUser
from .models import EmailEnterprise


@admin.register(EmailUser)
class EmailUserAdmin(UserAdmin):
    """
    EmailUser Model admin parameters.
    """

    add_form = EmailUserCreationForm
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_enterprise',
                                       'is_superuser', 'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login',)}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )

    list_display = ('email', 'first_name', 'last_name', 'created')
    ordering = ('id',)


@admin.register(EmailEnterprise)
class EmailEnterpriseAdmin(admin.ModelAdmin):
    """
    EmailEnterprise Model admin parameters.
    """

    list_display = ('email', 'name', 'city', 'country', 'address')
    ordering = ('id',)
