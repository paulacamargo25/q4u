import os
import environ


root = environ.Path(__file__) - 2
env = environ.Env(
    DEBUG=(bool, False), ALLOWED_HOSTS=(list, []), ENVIRONMENTS=(dict, {}))
environ.Env.read_env('%s/.env' % str(root - 1))

DEBUG = env.bool('DEBUG', default=True)

BASE_DIR = root()

SECRET_KEY = env('SECRET_KEY', default='')

ALLOWED_HOSTS = env.list('ALLOWED_HOSTS', default=[])

DATABASES = {
    'default': env.db()
}

DB_BACKUP_DIR = env(
    'DB_BACKUP_DIR', default=os.path.join(BASE_DIR, '../backups'))

ENVIRONMENTS = env.json('ENVIRONMENTS')

EMAIL_CONFIG = env.email_url('EMAIL_URL', default='consolemail://localhost:25')
EMAIL_BACKEND = EMAIL_CONFIG.get('EMAIL_BACKEND')
EMAIL_HOST = EMAIL_CONFIG.get('EMAIL_HOST')
EMAIL_HOST_PASSWORD = EMAIL_CONFIG.get('EMAIL_HOST_PASSWORD')
EMAIL_PORT = EMAIL_CONFIG.get('EMAIL_PORT')
EMAIL_FROM = env('EMAIL_FROM', default='example@example.com')

# Application definition

AUTH_USER_MODEL = 'accounts.EmailUser'

DJANGO_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.sites',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'corsheaders',
]

THIRD_PARTY_APPS = [
    'rest_framework',
    'django_extensions',
    'django_filters',
    'rest_framework.authtoken',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'allauth.socialaccount.providers.facebook',
]

OWN_APPS = []  # fabutils, log-viewer

PROJECT_APPS = [
    'accounts',
    'queues',
]

ENV_APPS = env.list('APPS', default=[])

INSTALLED_APPS = (
    DJANGO_APPS + THIRD_PARTY_APPS + OWN_APPS + PROJECT_APPS + ENV_APPS
)

LOGIN_REDIRECT_URL = "/"

AUTHENTICATION_BACKENDS = (
    'allauth.account.auth_backends.AuthenticationBackend',
    'django.contrib.auth.backends.ModelBackend',
)

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'corsheaders.middleware.CorsMiddleware',
]

ROOT_URLCONF = 'q4u.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'q4u.wsgi.application'


# Password validation
# https://docs.djangoproject.com/en/2.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.0/topics/i18n/

LANGUAGE_CODE = 'es'

TIME_ZONE = 'America/Lima'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.0/howto/static-files/

STATIC_URL = '/static/'

STATIC_ROOT = env(
    'STATIC_ROOT', default=os.path.join(BASE_DIR, '../static/'))

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.AllowAny',
    ),
    'DEFAULT_FILTER_BACKENDS': (
        'django_filters.rest_framework.DjangoFilterBackend',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.TokenAuthentication',
    ),
    'DEFAULT_METADATA_CLASS': 'rest_framework.metadata.SimpleMetadata'
}

LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)

SITE_ID = 1
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_USERNAME_REQUIRED = False
ACCOUNT_AUTHENTICATION_METHOD = 'email'
ACCOUNT_EMAIL_VERIFICATION = 'none'

CORS_ORIGIN_ALLOW_ALL = True
