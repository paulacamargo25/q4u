from django.contrib import admin
from django.urls import path, include

from accounts import views as accounts_views


api_urlpatterns = [
    path('accounts/', include('accounts.urls', namespace='accounts')),
    path('queues/', include('queues.urls', namespace='queues')),
    path(
        'logout/', accounts_views.logout, name='logout')
]

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include((api_urlpatterns, 'api'))),
]
