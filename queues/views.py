from rest_framework.response import Response
from rest_framework.exceptions import PermissionDenied
from rest_framework.viewsets import GenericViewSet, ModelViewSet
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import detail_route
from rest_framework import status

from accounts.models import EmailEnterprise
from .models import Queue, Ticket
from .serializers import (
    TicketSerializer, QueuesSerializer
)


class UserFilterView(GenericViewSet):
    """
    Base View that filters the models according to the request user
    """

    user_field = 'user'

    def get_logged_in_user(self):
        """
        Get the user query filter if there is a user logged in else return
        empty.

        :return: Dict with user filter.
        """

        if not self.request.user.is_anonymous:
            return {self.user_field: self.request.user}

        raise PermissionDenied(detail=None, code=None)

    def get_queryset(self):
        """
        Get the list of items for this view.

        :return: List of Transactions items.
        :rtype: Queryset
        """

        qs = super(UserFilterView, self).get_queryset().filter(
            **self.get_logged_in_user())
        return qs


class TicketViewSet(ModelViewSet, UserFilterView):
    serializer_class = TicketSerializer
    queryset = Ticket.objects.all()

    permission_classes = (IsAuthenticated, )

    def create(self, request):
        queue = Queue.objects.get(
            enterprise=request.data['enterprise'],
            queue_code=request.data['queue_code'])
        ticket = Ticket(
            queue=queue,
            num_ticket=queue.num_tickets_queue+1,
            user=self.request.user)
        ticket.save()
        queue.num_tickets_queue+=1
        queue.save()

        return Response({'data': ticket.id}, status=status.HTTP_201_CREATED)


class QueuesViewSet(ModelViewSet):

    permission_classes = (IsAuthenticated, )

    serializer_class = QueuesSerializer
    queryset = Queue.objects.all()

    def get_queryset(self):
        """
        Get the list of items for this view.

        :return: List of Transactions items.
        :rtype: Queryset
        """

        ent = EmailEnterprise.objects.get(user=self.request.user)
        qs = super(QueuesViewSet, self).get_queryset().filter(enterprise=ent)

        return qs

    def create(self, request):
        print(self.request.user)
        print(request.user)
        ent = EmailEnterprise.objects.get(user=self.request.user)
        request.data['enterprise'] = ent.pk
        request.data['queue_code'] = request.data['name'] + "_" + str(ent.pk)
        print(ent)
        print(request.data)

        return super(QueuesViewSet, self).create(request)

    @detail_route()
    def get_front_ticket(self, request, pk):
        queue = self.get_object()

        tic = Ticket.objects.get(queue=queue, num_ticket=queue.num_actual_ticket)

        tic.status = 'AG'
        queue.num_actual_ticket += 1
        queue.num_tickets_queue -= 1

        queue.save()
        tic.save()

        return Response()

