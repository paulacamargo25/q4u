# Generated by Django 2.0.1 on 2018-05-24 18:01

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django_extensions.db.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('accounts', '0002_emailenterprise'),
    ]

    operations = [
        migrations.CreateModel(
            name='Queue',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64, verbose_name='name')),
                ('num_actual_ticket', models.IntegerField(verbose_name='num actual ticket')),
                ('num_tickets_queue', models.IntegerField(verbose_name='num tickes queue')),
                ('enterprise', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accounts.EmailEnterprise', verbose_name='enterprise')),
            ],
            options={
                'verbose_name': 'Queue',
                'verbose_name_plural': 'Queues',
            },
        ),
        migrations.CreateModel(
            name='Ticket',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified')),
                ('status', models.CharField(choices=[('IQ', 'InQueue'), ('AG', 'Attending'), ('AD', 'Attended')], max_length=16, verbose_name='status')),
                ('num_ticket', models.IntegerField(verbose_name='num ticket')),
                ('queue', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='queues.Queue', verbose_name='queue')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='user')),
            ],
            options={
                'verbose_name': 'Ticket',
                'verbose_name_plural': 'Tickets',
            },
        ),
    ]
