# Generated by Django 2.0.1 on 2018-06-08 22:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0004_auto_20180525_1824'),
        ('queues', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='queue',
            name='queue_code',
            field=models.CharField(default='code1', max_length=16, verbose_name='queue_code'),
        ),
        migrations.AlterUniqueTogether(
            name='queue',
            unique_together={('enterprise', 'queue_code')},
        ),
    ]
