from .views import TicketViewSet, QueuesViewSet
from rest_framework.routers import DefaultRouter

app_name = 'queues'

router = DefaultRouter()
router.register(r'tickets', TicketViewSet, base_name='tickets')
router.register(r'queues', QueuesViewSet, base_name='queues')


urlpatterns = router.urls
