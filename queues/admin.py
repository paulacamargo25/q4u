from django.contrib import admin
from .models import Queue, Ticket

#admin.site.register(Queue)
#admin.site.register(Ticket)

@admin.register(Queue)
class QueueAdmin (admin.ModelAdmin):
    list_display = ('id', 'num_actual_ticket', 'num_tickets_queue')
    ordering = ('id',)

@admin.register(Ticket)
class TicketAdmin (admin.ModelAdmin):
    list_display = ('id', 'user', 'queue', 'status', 'num_ticket')
    ordering = ('id',)

