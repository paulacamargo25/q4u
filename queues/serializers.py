from rest_framework import serializers

from .models import Queue, Ticket


class QueueSerializer(serializers.ModelSerializer):
    class Meta:
        model = Queue
        fields = ('queue_code', 'num_actual_ticket')


class TicketSerializer(serializers.ModelSerializer):
    queue = serializers.SlugRelatedField(read_only=True, slug_field='queue_code')

    class Meta:
        model = Ticket
        fields = ('id', 'user', 'queue', 'status', 'num_ticket')
        read_only_fields = ('id', 'status',)


class QueuesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Queue
        fields = (
            'id', 'name', 'queue_code', 'num_tickets_queue',
            'num_actual_ticket', 'enterprise')
        read_only_fields = ('id', 'num_tickets_queue', 'num_actual_ticket')