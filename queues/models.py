from django.db import models
from django.utils.translation import gettext_lazy as _
from django_extensions.db.models import TimeStampedModel
from accounts.models import EmailUser, EmailEnterprise


class Queue (models.Model):
    name = models.CharField(
        verbose_name=_('name'),
        max_length=64,
        blank=False
    )
    enterprise = models.ForeignKey(EmailEnterprise,
        verbose_name=_('enterprise'),
        on_delete=models.CASCADE,
    )
    num_actual_ticket = models.IntegerField(
        verbose_name=_('num actual ticket'),
        default=0
    )
    num_tickets_queue = models.IntegerField(
        verbose_name=_('num tickes queue'),
        default=0
    )

    queue_code = models.CharField(
        verbose_name=_('queue_code'),
        max_length=16,
        default='code1'
    )

    def __str__(self):
        return self.queue_code

    class Meta:
        verbose_name = _('Queue')
        verbose_name_plural = _('Queues')
        unique_together = ('enterprise', 'queue_code')


class Ticket (TimeStampedModel):
    user = models.ForeignKey(
        EmailUser,verbose_name=_('user'), on_delete=models.CASCADE,)
    queue = models.ForeignKey(
        Queue, verbose_name=_('queue'), on_delete=models.CASCADE)
    InQueue = 'IQ'
    Attending = 'AG'
    Attended = 'AD'
    status_choices = (
        (InQueue, 'InQueue'),
        (Attending, 'Attending'),
        (Attended, 'Attended'),
    )
    status = models.CharField(
        verbose_name=_('status'),
        choices=status_choices,
        max_length=16,
        blank=False,
        default=InQueue,
    )
    num_ticket = models.IntegerField(
        verbose_name=_('num ticket'),
    )
    #
    # def __str__ (self):
    #     return self.num_ticket

    class Meta:
        verbose_name = _('Ticket')
        verbose_name_plural = _('Tickets')
